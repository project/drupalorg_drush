For more detail on PHPUnit and Drush, see the README in the Drush `tests` directory.

To run these tests:
1. Add to path/to/drush/tests/phpunit.xml:
   <testsuite name="Drupalorg">
     <directory>path/to/drupalorg_drush/tests</directory>
   </testsuite>

2. Use the following command:
   path/to/drush/vendor/bin/phpunit --configuration="path/to/drush/tests" --testsuite=Drupalorg
