# Drupal.org drush
User-facing drush commands that are specific to the Drupal.org site.

Currently, this includes the extensions to drush make that are used by the Drupal.org distribution packaging system. They live here as a separate project to make it easy for potential distribution maintainers to get this additional validation code so they can ensure that their distribution .make file is valid for the Drupal.org packaging system.

## Installation

1. Download the module using drush:
   ```
   drush dl drupalorg_drush
   ```
1. Verify that the folder drupalorg_drush exists under .drush:
   ```
   ls ~/.drush
   ```
1. If the drupalorg_drush folder is NOT under your .drush folder, you'll need to move it there manually.
